# Spring Test #

Springの簡易サンプルです。  
対象: 初心者  
最低限の動作がなんとなく理解できるようになることが目標となっています。  

## 必要ツール ##
* Java 8
* 好きなIDE/テキストエディタ
* Git
* Tomcat 8
* Gradle (ラッパーを使うのでインストールの必要なし)

## ツールの説明 ##
* ***Git*** バージョン管理に使います
* ***Tomcat*** サーブレットを動かすためのサーバーです
* ***Gradle*** WARファイル(アプリケーションを1ファイルにまとめたもの)を作るのに使います。

## 作業手順 ##
1. ファイルをリポジトリから取得する  
`git clone https://takutokoyama@bitbucket.org/takutokoyama/springtest.git`
2. 各ファイルの意味を理解する(下記参照)
3. WARファイルを作成する `gradlew war`  
出力先はbuild/libs/
4. Tomcatにデプロイ
5. ブラウザで[http://localhost:8080](http://localhost:8080)にアクセス
6. パラメーターを渡してみる: [http://localhost:8080?name=your_name]()

## ファイル解説 ##
* build.gradle
* Application.java
* GreetingController.java
* User.java
* greeting.jsp

### build.gradle ###
Gradleのビルドスクリプトです。  
Spring用の設定がなされています。

### Application.java ###
SpringBootでアプリケーションを起動するためのクラスです。

### GreetingController ###
HTTPリクエストを受け取り処理するコントローラークラスです。

### greeting.jsp ###
ビューを担当するJSPファイルです。  
JSTLを使うことも出来ます。

各ファイルのコメントを参照
TODO
