<%--JSTLを有効化--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--JSPは出力時にもエンコードを指定しなければならない--%>
<%@ page contentType="text/html;charset=UTF-8" %>

<html>

<head>
    <title>Getting Started: Serving Web Content</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
</head>

<body>

<%--エラーメッセージ: JSTLを使用しエラー時のみメッセージが表示される--%>
<c:if test="${error}"><h1>error</h1></c:if>

<%--モデルにバインドされた${name}が表示されます--%>
<h1>hello, ${name}!</h1>
<p><a href="./greeting?name=John">./greeting?name=John</a></p>
<p><a href="./greeting?name=e">./greeting?name=e</a></p>

</body>

</html>
