package jp.cp.wellstone.springsample;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.util.Map;

/**
 * 挨拶を返すコントローラー
 */
@Controller
public class GreetingController {

    /**
     * ユーザー名を受け取ると挨拶します
     */
    // "/"もしくは"/greeting"にブラウザーからアクセスしたとき、このメソッドが呼び出される
    @RequestMapping({"/", "/greeting"})
    public String greeting(
            // Springでパラメーターを受け取る方法はいくつかあり、直接受け取ったり
            // Mapで受け取ったり、クラスで受け取ったりできます。
//            @RequestParam(value = "name", required = false, defaultValue = "world") String name,
//            @RequestParam Map<String, String> param,
            // @Validアノテーションをつけると、BeanValidatorの仕様に従い
            // 引数クラスの値が正当か検証できます。
            @Valid User param,
            // 直前のパラメーターが(この例だとparam)を検証するためのインスタンスです
            BindingResult bindingResult,
            // Viewに渡すモデルを定義します
            Model model) {

        // paramにエラーがあった場合
        if (bindingResult.hasErrors()) {
            model.addAttribute("error", true);
            return "greeting";
        }

        // デフォルト値をworldに設定
        String name = param.getName() != null ? param.getName() : "world";

        // モデルにnameを設定し、ビューで受け取れるようにする
        model.addAttribute("name", name);

        // ビューの名前を戻り値として指定する
        return "greeting";
    }

}

