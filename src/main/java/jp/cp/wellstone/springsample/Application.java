package jp.cp.wellstone.springsample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;

import java.util.HashMap;
import java.util.Map;

/**
 * SpringBootを使用してアプリケーションを起動するためのクラスです。<br />
 * SpringBootServletInitializerはWARファイルとして展開する際に継承が必要になるクラスです。
 */
@SpringBootApplication
public class Application extends SpringBootServletInitializer {

    /**
     * WARファイル作成時の設定を定義します
     */
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {

        // JSPファイルのパターンを指定
        Map<String, Object> properties = new HashMap<>();
        properties.put("spring.mvc.view.prefix", "/");
        properties.put("spring.mvc.view.suffix", ".jsp");

        return application.properties(properties)
                          .sources(Application.class);
    }

    /**
     * Springアプリケーションを起動します
     */
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

}

