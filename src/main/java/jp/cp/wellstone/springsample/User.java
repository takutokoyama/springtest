package jp.cp.wellstone.springsample;

import javax.validation.constraints.Size;

/**
 * ユーザーを表すBeanクラスです
 */
public class User {

    /**
     * ユーザー名<br />
     * 文字数は最低2
     */
    @Size(min = 2)
    private String name;

    /**
     * ゲッター
     */
    public String getName() {
        return name;
    }

    /**
     * セッター
     */
    public void setName(String name) {
        this.name = name;
    }

}

